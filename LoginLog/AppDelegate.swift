//
//  AppDelegate.swift
//  LoginLogNG
//
//  Created by Timothy Perfitt on 3/22/21.
//  Copyright © 2021 Göteborgs universitet. All rights reserved.
//

import Cocoa

@main
class App {
    static func main() {
        if CommandLine.arguments.contains("-w"){
            sleep(5)
        }
        _ = NSApplicationMain(CommandLine.argc, CommandLine.unsafeArgv)
    }
}
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var logWindowController: LLLogWindowController!
    @IBOutlet var window: NSWindow!

    var logfile = ""
    var style = ""
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        UserDefaults.standard.register(defaults:["logfile":"/var/log/install.log","style":"focused"])

        logfile=UserDefaults.standard.string(forKey: "logfile") ?? "/var/log/install.log"
        style=UserDefaults.standard.string(forKey: "style") ?? "focused"
        self.logWindowController.showLogWindow(title: logfile, style: style)
        self.logWindowController.watchLogFile(logFile: logfile)
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

