//
//  LLLogWindowController.swift
//  LoginLogNG
//
//  Created by Timothy Perfitt on 3/22/21.
//  Copyright © 2021 Göteborgs universitet. All rights reserved.
//

import Cocoa

class LLLogViewDataSource: NSObject,NSTableViewDataSource, NSTableViewDelegate {
    var logFileData = Array<String>()
    var logFileColor = Array<NSColor>()
    var lastLineIsPartial = false


    func addLine(line:String, isPartial:Bool)  {
        if lastLineIsPartial==true{
            let (newLine, color) = parseLineAttr(line: logFileData.last!+line)
            logFileData.removeLast()
            logFileColor.removeLast()
            logFileData.append(newLine)
            logFileColor.append(color)
        }
        else {

            let (newLine, color) = parseLineAttr(line: line)
            logFileData.append(newLine)
            logFileColor.append(color)

        }
        lastLineIsPartial=isPartial
    }

    
    func tableView(_ tableView: NSTableView, objectValueFor tableColumn: NSTableColumn?, row: Int) -> Any? {
        return logFileData[row]

    }
    func tableView(_ tableView: NSTableView, dataCellFor tableColumn: NSTableColumn?, row: Int) -> NSCell? {
        if tableColumn != nil {
            let cell=tableColumn?.dataCell(forRow: row) as? NSTextFieldCell
            cell?.textColor=logFileColor[row]
            return cell
        }
        return nil
    }



    func parseLineAttr(line:String) -> (String,NSColor) {
        return (line,NSColor.labelColor)
    }
    func removeAllLines()  {
        logFileData.removeAll()
    }

    func lineCount() -> Int {
        return logFileData.count
    }

    func numberOfRows(in tableView: NSTableView) -> Int {
        return lineCount()
    }

    

//

}
class LLLogWindowController: NSWindowController {

    @IBOutlet weak var logView: NSTableView!
    @IBOutlet weak var backdropWindow: LLBorderlessWindow!
    var logFileData = LLLogViewDataSource()
    var filehandle:FileHandle?
    var updateTimer:Timer?
    @IBOutlet weak var scrollToMostRecentButton: NSButton!

    override func awakeFromNib() {
        NotificationCenter.default.addObserver(
            forName: NSApplication.didChangeScreenParametersNotification,
            object: nil,
            queue: OperationQueue.main
        ) { notification -> Void in
            let style=UserDefaults.standard.string(forKey: "style") ?? "focused"

            self.setupWindow(style: style)
        }
    }
    @IBOutlet weak var tableColumn: NSTableColumn!
    func showLogWindow(title:String,style:String) {
        logView.delegate=logFileData
        self.window?.title=title

        setupWindow(style:style)

    }



    func setupWindow(style:String)  {
        //
        //         # Base all sizes on the screen's dimensions.
                let screenRect = NSScreen.main?.frame
        //
        //         # Open a log window that covers most of the screen.
                self.window?.canBecomeVisibleWithoutLogin=true
                self.window?.level=NSWindow.Level.screenSaver
                self.window?.orderFrontRegardless()
        self.window?.makeKeyAndOrderFront(self)
        self.window?.orderFront(self)
        NSApp.activate(ignoringOtherApps: true)
        //         # Resize the log window so that it leaves a border on all sides.
        //         # Add a little extra border at the bottom so we don't cover the
        //         # loginwindow message.
                var windowRect=screenRect
                windowRect?.origin.x=100.0
                windowRect?.origin.y=200.0
                windowRect?.size.width-=200.0
                windowRect?.size.height-=300.0

                 NSAnimationContext.beginGrouping()
                NSAnimationContext.current.duration=0.1
                if let rect=windowRect {
                    self.window?.animator().setFrame(rect, display: true)
                }
                 NSAnimationContext.endGrouping()
                if style=="focused"{
                    self.backdropWindow.canBecomeVisibleWithoutLogin=true
                    self.backdropWindow.level=NSWindow.Level.statusBar
                    if let sr = screenRect {

                        self.backdropWindow.setFrame(sr, display: true)
                    }
                    let translucentColor = NSColor.black.withAlphaComponent(0.75)
                    self.backdropWindow.backgroundColor=translucentColor
                    self.backdropWindow.isOpaque=false
                    self.backdropWindow.ignoresMouseEvents=false
                    self.backdropWindow.alphaValue=0.0
                    self.backdropWindow.orderFrontRegardless()
                    NSAnimationContext.beginGrouping()
                    NSAnimationContext.current.duration=1.0
                    self.backdropWindow.animator().alphaValue=1.0
                    NSAnimationContext.endGrouping()
                }
                tableColumn.width=10000

    }
    func watchLogFile(logFile:String)  {
        stopWatching()
        logFileData.removeAllLines()
        logView.dataSource=logFileData
        logView.reloadData()
        filehandle = FileHandle.init(forReadingAtPath: logFile)
        refreshLog()
        updateTimer=Timer.scheduledTimer(timeInterval: 0.25, target: self, selector: #selector(refreshLog), userInfo: nil, repeats: true)
    }
    func stopWatching()  {
        if filehandle != nil {

            filehandle?.closeFile()
            filehandle=nil
        }
        if updateTimer != nil {

            updateTimer?.invalidate()
            updateTimer=nil
        }

    }
    
    @objc func refreshLog()  {
        let data = self.filehandle?.availableData
        if let d = data, d.count>0,let string = String.init(data: d, encoding: .utf8){

            for line in string.components(separatedBy: "\n"){
                self.logFileData.addLine(line: line, isPartial: false)

//                if line.hasSuffix("\n"){
//                    self.logFileData.addLine(line: line, isPartial: false)
//                }
//                else {
//                    logFileData.addLine(line: line, isPartial: true)
//                }
            }

        }
        logView.reloadData()
        if scrollToMostRecentButton.state == .on {
            logView.scrollRowToVisible(logFileData.lineCount()-1)
        }
    }

}
